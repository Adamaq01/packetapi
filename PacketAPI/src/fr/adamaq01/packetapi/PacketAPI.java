package fr.adamaq01.packetapi;

import java.util.ArrayList;

import org.bukkit.entity.Player;
import org.bukkit.plugin.java.JavaPlugin;

import fr.adamaq01.packetapi.packets.Packet;
import fr.adamaq01.packetapi.packets.enums.PacketType;
import fr.adamaq01.packetapi.packets.events.PacketEvent;
import fr.adamaq01.packetapi.utils.PacketHandler;
import fr.adamaq01.packetapi.utils.PacketInjector;
import fr.adamaq01.packetapi.utils.Reflection;

public class PacketAPI extends JavaPlugin {

	private ArrayList<PacketHandler<?>> handlers = new ArrayList<>();
	private PacketInjector injector;
	private static PacketAPI instance;

	@Override
	public void onEnable() {
		instance = this;
		injector = new PacketInjector();
		getServer().getPluginManager().registerEvents(new PlayerJoinAndQuitEvents(), this);
		for (Player pls : getServer().getOnlinePlayers()) {
			injector.addPlayer(pls);
		}
	}

	@Override
	public void onDisable() {
		handlers.clear();
		for (Player pls : getServer().getOnlinePlayers()) {
			injector.removePlayer(pls);
		}
	}

	@SuppressWarnings({ "rawtypes", "unchecked" })
	public PacketEvent onPacket(Player p, Packet packet) {
		PacketEvent<?> event = new PacketEvent<>(packet, p);
		for (PacketHandler h : handlers) {
			if (event.getPacket().getPacketType().equals(h.getPacketType())
					&& !h.getPacketType().equals(PacketType.In.ALL)) {
				h.onPacket(event);
			}
			if (h.getPacketType().equals(PacketType.In.ALL)
					&& event.getPacket().getPacketType().getName().contains("In")) {
				h.onPacket(event);
			}
			if (h.getPacketType().equals(PacketType.Out.ALL)
					&& event.getPacket().getPacketType().getName().contains("Out")) {
				h.onPacket(event);
			}
		}
		return event;
	}

	public static void addPacketListener(PacketHandler<?> handler) {
		getInstance().getHandlers().add(handler);
	}

	public static void removePacketListener(PacketHandler<?> handler) {
		getInstance().getHandlers().remove(handler);
	}

	public static boolean sendPacket(Player player, Packet packet) {
		try {
			Reflection.sendPlayerPacket(player, packet);
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
	}

	public static PacketAPI getInstance() {
		return instance;
	}

	public ArrayList<PacketHandler<?>> getHandlers() {
		return handlers;
	}

	public PacketInjector getInjector() {
		return injector;
	}

}
