package fr.adamaq01.packetapi;

import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerQuitEvent;

public class PlayerJoinAndQuitEvents implements Listener {

	@EventHandler
	public void onJoin(PlayerJoinEvent e) {
		PacketAPI.getInstance().getInjector().addPlayer(e.getPlayer());
	}

	@EventHandler
	public void onQuit(PlayerQuitEvent e) {
		PacketAPI.getInstance().getInjector().removePlayer(e.getPlayer());
	}

}
