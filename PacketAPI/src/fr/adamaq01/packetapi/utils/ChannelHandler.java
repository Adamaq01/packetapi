package fr.adamaq01.packetapi.utils;

import org.bukkit.entity.Entity;
import org.bukkit.entity.Player;

import fr.adamaq01.packetapi.PacketAPI;
import fr.adamaq01.packetapi.packets.Packet;
import fr.adamaq01.packetapi.packets.enums.Animation;
import fr.adamaq01.packetapi.packets.enums.EntityUseAction;
import fr.adamaq01.packetapi.packets.enums.PacketType;
import fr.adamaq01.packetapi.packets.enums.ResourcePackStatus;
import fr.adamaq01.packetapi.packets.events.PacketEvent;
import fr.adamaq01.packetapi.packets.in.PacketPlayInArmAnimation;
import fr.adamaq01.packetapi.packets.in.PacketPlayInBlockDig;
import fr.adamaq01.packetapi.packets.in.PacketPlayInBlockPlace;
import fr.adamaq01.packetapi.packets.in.PacketPlayInChat;
import fr.adamaq01.packetapi.packets.in.PacketPlayInFlying;
import fr.adamaq01.packetapi.packets.in.PacketPlayInHeldItemSlot;
import fr.adamaq01.packetapi.packets.in.PacketPlayInKeepAlive;
import fr.adamaq01.packetapi.packets.in.PacketPlayInLook;
import fr.adamaq01.packetapi.packets.in.PacketPlayInPosition;
import fr.adamaq01.packetapi.packets.in.PacketPlayInPositionLook;
import fr.adamaq01.packetapi.packets.in.PacketPlayInResourcePackStatus;
import fr.adamaq01.packetapi.packets.in.PacketPlayInUpdateSign;
import fr.adamaq01.packetapi.packets.in.PacketPlayInUseEntity;
import fr.adamaq01.packetapi.packets.out.PacketPlayOutAnimation;
import fr.adamaq01.packetapi.packets.out.PacketPlayOutCamera;
import fr.adamaq01.packetapi.packets.out.PacketPlayOutOpenSignEditor;
import io.netty.channel.ChannelDuplexHandler;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.ChannelPromise;

public class ChannelHandler extends ChannelDuplexHandler {

	private Player player;

	public ChannelHandler(Player p) {
		this.player = p;
	}

	@SuppressWarnings("unchecked")
	@Override
	public void write(ChannelHandlerContext c, Object m, ChannelPromise promise) throws Exception {
		if (m.getClass().getSimpleName().equalsIgnoreCase("PacketPlayOutAnimation")) {
			PacketPlayOutAnimation p = null;
			int pl = (int) Reflection.getFieldValue(m, "a");
			if (Reflection.getFieldValue(m, "b").equals(0)) {
				p = new PacketPlayOutAnimation(pl, Animation.SWING_ARM, m);
			} else if (Reflection.getFieldValue(m, "b").equals(1)) {
				p = new PacketPlayOutAnimation(pl, Animation.TAKE_DAMAGE, m);
			} else if (Reflection.getFieldValue(m, "b").equals(2)) {
				p = new PacketPlayOutAnimation(pl, Animation.LEAVE_BED, m);
			} else if (Reflection.getFieldValue(m, "b").equals(3)) {
				p = new PacketPlayOutAnimation(pl, Animation.EAT_FOOD, m);
			} else if (Reflection.getFieldValue(m, "b").equals(4)) {
				p = new PacketPlayOutAnimation(pl, Animation.CRITICAL_EFFECT, m);
			} else if (Reflection.getFieldValue(m, "b").equals(5)) {
				p = new PacketPlayOutAnimation(pl, Animation.MAGIC_CRITICAL_EFFECT, m);
			}
			PacketEvent<PacketPlayOutAnimation> pe = PacketAPI.getInstance().onPacket(player, p);
			if (!pe.isCancelled()) {
				super.write(c, m, promise);
			}
		} else if (m.getClass().getSimpleName().equalsIgnoreCase("PacketPlayOutCamera")) {
			PacketPlayOutCamera p = null;
			for (Entity ent : player.getWorld().getEntities()) {
				if (ent.getEntityId() == (int) Reflection.getFieldValue(m, "a")) {
					p = new PacketPlayOutCamera(ent, m);
					break;
				}
			}
			PacketEvent<PacketPlayOutCamera> pe = PacketAPI.getInstance().onPacket(player, p);
			if (!pe.isCancelled()) {
				super.write(c, m, promise);
			}
		} else if (m.getClass().getSimpleName().equalsIgnoreCase("PacketPlayOutOpenSignEditor")) {
			PacketPlayOutOpenSignEditor p = new PacketPlayOutOpenSignEditor(
					Reflection.getLocationFromBlockPosition(Reflection.getFieldValue(m, "a"), player.getWorld()), m);
			PacketEvent<PacketPlayOutOpenSignEditor> pe = PacketAPI.getInstance().onPacket(player, p);
			if (!pe.isCancelled()) {
				super.write(c, m, promise);
			}
		} /*
			 * else if (m.getClass().getSimpleName().equalsIgnoreCase(
			 * "PacketPlayOutUpdateSign")) { net.minecraft.server.v1_8_R2.World
			 * w = (World) Reflection.getFieldValue(m, "a"); org.bukkit.World w2
			 * = w.getWorld(); PacketPlayOutUpdateSign p = new
			 * PacketPlayOutUpdateSign(w2, (BlockPosition)
			 * Reflection.getFieldValue(m, "b"), (IChatBaseComponent[])
			 * Reflection.getFieldValue(m, "c")); PacketEvent pe =
			 * PacketAPI.getInstance().onPacket(player, p); if
			 * (!pe.isCancelled()) { super.write(c, m, promise); } }
			 */ else {
			Packet p = new Packet(PacketType.Out.ALL, m);
			PacketEvent<Packet> pe = PacketAPI.getInstance().onPacket(player, p);
			if (!pe.isCancelled()) {
				super.write(c, m, promise);
			}
		}
	}

	@SuppressWarnings("unchecked")
	@Override
	public void channelRead(ChannelHandlerContext c, Object m) throws Exception {
		if (m.getClass().getSimpleName().equalsIgnoreCase("PacketPlayInChat")) {
			PacketPlayInChat p = new PacketPlayInChat((String) Reflection.getFieldValue(m, "a"), m);
			PacketEvent<PacketPlayInChat> pe = PacketAPI.getInstance().onPacket(player, p);
			if (!pe.isCancelled()) {
				super.channelRead(c, m);
			}
		} else if (m.getClass().getSimpleName().equalsIgnoreCase("PacketPlayInKeepAlive")) {
			PacketPlayInKeepAlive p = new PacketPlayInKeepAlive((int) Reflection.getFieldValue(m, "a"), m);
			PacketEvent<PacketPlayInKeepAlive> pe = PacketAPI.getInstance().onPacket(player, p);
			if (!pe.isCancelled()) {
				super.channelRead(c, m);
			}
		} else if (m.getClass().getSimpleName().equalsIgnoreCase("PacketPlayInUseEntity")) {
			PacketPlayInUseEntity p = null;
			if (Reflection.getFieldValue(m, "action").toString().equals("ATTACK")) {
				p = new PacketPlayInUseEntity((int) Reflection.getFieldValue(m, "a"), EntityUseAction.ATTACK, m);
			} else if (Reflection.getFieldValue(m, "action").toString().equals("INTERACT")) {
				p = new PacketPlayInUseEntity((int) Reflection.getFieldValue(m, "a"), EntityUseAction.INTERACT, m);
			} else if (Reflection.getFieldValue(m, "action").toString().equals("INTERACT_AT")) {
				p = new PacketPlayInUseEntity((int) Reflection.getFieldValue(m, "a"), EntityUseAction.INTERACT_AT, m);
			}
			PacketEvent<PacketPlayInUseEntity> pe = PacketAPI.getInstance().onPacket(player, p);
			if (!pe.isCancelled()) {
				super.channelRead(c, m);
			}
		} else if (m.getClass().getSimpleName().equalsIgnoreCase("PacketPlayInPosition")) {
			PacketPlayInPosition p = new PacketPlayInPosition((double) Reflection.getProtectedFieldValue(m, "x"),
					(double) Reflection.getProtectedFieldValue(m, "y"), (double) Reflection.getProtectedFieldValue(m, "z"),
					(float) Reflection.getProtectedFieldValue(m, "yaw"), (float) Reflection.getProtectedFieldValue(m, "pitch"),
					(boolean) Reflection.getProtectedFieldValue(m, "f"), (boolean) Reflection.getProtectedFieldValue(m, "hasLook"),
					(boolean) Reflection.getProtectedFieldValue(m, "hasPos"), m);
			PacketEvent<PacketPlayInPosition> pe = PacketAPI.getInstance().onPacket(player, p);
			if (!pe.isCancelled()) {
				super.channelRead(c, m);
			}
		} else if (m.getClass().getSimpleName().equalsIgnoreCase("PacketPlayInLook")) {
			PacketPlayInLook p = new PacketPlayInLook((double) Reflection.getProtectedFieldValue(m, "x"),
					(double) Reflection.getProtectedFieldValue(m, "y"), (double) Reflection.getProtectedFieldValue(m, "z"),
					(float) Reflection.getProtectedFieldValue(m, "yaw"), (float) Reflection.getProtectedFieldValue(m, "pitch"),
					(boolean) Reflection.getProtectedFieldValue(m, "f"), (boolean) Reflection.getProtectedFieldValue(m, "hasLook"),
					(boolean) Reflection.getProtectedFieldValue(m, "hasPos"), m);
			PacketEvent<PacketPlayInLook> pe = PacketAPI.getInstance().onPacket(player, p);
			if (!pe.isCancelled()) {
				super.channelRead(c, m);
			}
		} else if (m.getClass().getSimpleName().equalsIgnoreCase("PacketPlayInPositionLook")) {
			PacketPlayInPositionLook p = new PacketPlayInPositionLook((double) Reflection.getProtectedFieldValue(m, "x"),
					(double) Reflection.getProtectedFieldValue(m, "y"), (double) Reflection.getProtectedFieldValue(m, "z"),
					(float) Reflection.getProtectedFieldValue(m, "yaw"), (float) Reflection.getProtectedFieldValue(m, "pitch"),
					(boolean) Reflection.getProtectedFieldValue(m, "f"), (boolean) Reflection.getProtectedFieldValue(m, "hasLook"),
					(boolean) Reflection.getProtectedFieldValue(m, "hasPos"), m);
			PacketEvent<PacketPlayInPositionLook> pe = PacketAPI.getInstance().onPacket(player, p);
			if (!pe.isCancelled()) {
				super.channelRead(c, m);
			}
		} else if (m.getClass().getSimpleName().equalsIgnoreCase("PacketPlayInFlying")) {
			PacketPlayInFlying p = new PacketPlayInFlying((double) Reflection.getFieldValue(m, "x"),
					(double) Reflection.getFieldValue(m, "y"), (double) Reflection.getFieldValue(m, "z"),
					(float) Reflection.getFieldValue(m, "yaw"), (float) Reflection.getFieldValue(m, "pitch"),
					(boolean) Reflection.getFieldValue(m, "f"), (boolean) Reflection.getFieldValue(m, "hasLook"),
					(boolean) Reflection.getFieldValue(m, "hasPos"), m);
			PacketEvent<PacketPlayInFlying> pe = PacketAPI.getInstance().onPacket(player, p);
			if (!pe.isCancelled()) {
				super.channelRead(c, m);
			}
		} else if (m.getClass().getSimpleName().equalsIgnoreCase("PacketPlayInUpdateSign")) {
			PacketPlayInUpdateSign p = new PacketPlayInUpdateSign(
					Reflection.getStringFromIChatBaseComponent((Object[]) Reflection.getFieldValue(m, "b")),
					Reflection.getLocationFromBlockPosition(Reflection.getFieldValue(m, "a"), player.getWorld()), m);
			PacketEvent<PacketPlayInUpdateSign> pe = PacketAPI.getInstance().onPacket(player, p);
			if (!pe.isCancelled()) {
				super.channelRead(c, m);
			}
		} else if (m.getClass().getSimpleName().equalsIgnoreCase("PacketPlayInBlockDig")) {
			PacketPlayInBlockDig p = new PacketPlayInBlockDig(
					Reflection.getLocationFromBlockPosition(Reflection.getFieldValue(m, "a"), player.getWorld()), null,
					null, m);
			/*
			 * System.out.println(Reflection.getFieldValue(m, "a").toString() +
			 * " : " + Reflection.getFieldValue(m, "b") + " : " +
			 * Reflection.getFieldValue(m, "c")); if
			 * (Reflection.getFieldValue(m, "b").equals("up")) { if
			 * (Reflection.getFieldValue(m, "c").equals("START_DESTROY_BLOCK"))
			 * { p = new PacketPlayInBlockDig((BlockPosition)
			 * Reflection.getFieldValue(m, "a"), Direction.DOWN,
			 * DigStatus.START_DESTROY_BLOCK); } else if
			 * (Reflection.getFieldValue(m, "c").equals("ABORT_DESTROY_BLOCK"))
			 * { p = new PacketPlayInBlockDig((BlockPosition)
			 * Reflection.getFieldValue(m, "a"), Direction.DOWN,
			 * DigStatus.ABORT_DESTROY_BLOCK); } else if
			 * (Reflection.getFieldValue(m, "c").equals("STOP_DESTROY_BLOCK")) {
			 * p = new PacketPlayInBlockDig((BlockPosition)
			 * Reflection.getFieldValue(m, "a"), Direction.DOWN,
			 * DigStatus.STOP_DESTROY_BLOCK); } else if
			 * (Reflection.getFieldValue(m, "c").equals("DROP_ALL_ITEMS")) { p =
			 * new PacketPlayInBlockDig((BlockPosition)
			 * Reflection.getFieldValue(m, "a"), Direction.DOWN,
			 * DigStatus.DROP_ALL_ITEMS); } else if (Reflection.getFieldValue(m,
			 * "c").equals("DROP_ITEM")) { p = new
			 * PacketPlayInBlockDig((BlockPosition) Reflection.getFieldValue(m,
			 * "a"), Direction.DOWN, DigStatus.DROP_ITEM); } else if
			 * (Reflection.getFieldValue(m, "c").equals("RELEASE_USE_ITEM")) { p
			 * = new PacketPlayInBlockDig((BlockPosition)
			 * Reflection.getFieldValue(m, "a"), Direction.DOWN,
			 * DigStatus.RELEASE_USE_ITEM); } } else if
			 * (Reflection.getFieldValue(m, "b").equals("down")) { if
			 * (Reflection.getFieldValue(m, "c").equals("START_DESTROY_BLOCK"))
			 * { p = new PacketPlayInBlockDig((BlockPosition)
			 * Reflection.getFieldValue(m, "a"), Direction.UP,
			 * DigStatus.START_DESTROY_BLOCK); } else if
			 * (Reflection.getFieldValue(m, "c").equals("ABORT_DESTROY_BLOCK"))
			 * { p = new PacketPlayInBlockDig((BlockPosition)
			 * Reflection.getFieldValue(m, "a"), Direction.UP,
			 * DigStatus.ABORT_DESTROY_BLOCK); } else if
			 * (Reflection.getFieldValue(m, "c").equals("STOP_DESTROY_BLOCK")) {
			 * p = new PacketPlayInBlockDig((BlockPosition)
			 * Reflection.getFieldValue(m, "a"), Direction.UP,
			 * DigStatus.STOP_DESTROY_BLOCK); } else if
			 * (Reflection.getFieldValue(m, "c").equals("DROP_ALL_ITEMS")) { p =
			 * new PacketPlayInBlockDig((BlockPosition)
			 * Reflection.getFieldValue(m, "a"), Direction.UP,
			 * DigStatus.DROP_ALL_ITEMS); } else if (Reflection.getFieldValue(m,
			 * "c").equals("DROP_ITEM")) { p = new
			 * PacketPlayInBlockDig((BlockPosition) Reflection.getFieldValue(m,
			 * "a"), Direction.UP, DigStatus.DROP_ITEM); } else if
			 * (Reflection.getFieldValue(m, "c").equals("RELEASE_USE_ITEM")) { p
			 * = new PacketPlayInBlockDig((BlockPosition)
			 * Reflection.getFieldValue(m, "a"), Direction.UP,
			 * DigStatus.RELEASE_USE_ITEM); } } else if
			 * (Reflection.getFieldValue(m, "b").equals("west")) { if
			 * (Reflection.getFieldValue(m, "c").equals("START_DESTROY_BLOCK"))
			 * { p = new PacketPlayInBlockDig((BlockPosition)
			 * Reflection.getFieldValue(m, "a"), Direction.EAST,
			 * DigStatus.START_DESTROY_BLOCK); } else if
			 * (Reflection.getFieldValue(m, "c").equals("ABORT_DESTROY_BLOCK"))
			 * { p = new PacketPlayInBlockDig((BlockPosition)
			 * Reflection.getFieldValue(m, "a"), Direction.EAST,
			 * DigStatus.ABORT_DESTROY_BLOCK); } else if
			 * (Reflection.getFieldValue(m, "c").equals("STOP_DESTROY_BLOCK")) {
			 * p = new PacketPlayInBlockDig((BlockPosition)
			 * Reflection.getFieldValue(m, "a"), Direction.EAST,
			 * DigStatus.STOP_DESTROY_BLOCK); } else if
			 * (Reflection.getFieldValue(m, "c").equals("DROP_ALL_ITEMS")) { p =
			 * new PacketPlayInBlockDig((BlockPosition)
			 * Reflection.getFieldValue(m, "a"), Direction.EAST,
			 * DigStatus.DROP_ALL_ITEMS); } else if (Reflection.getFieldValue(m,
			 * "c").equals("DROP_ITEM")) { p = new
			 * PacketPlayInBlockDig((BlockPosition) Reflection.getFieldValue(m,
			 * "a"), Direction.EAST, DigStatus.DROP_ITEM); } else if
			 * (Reflection.getFieldValue(m, "c").equals("RELEASE_USE_ITEM")) { p
			 * = new PacketPlayInBlockDig((BlockPosition)
			 * Reflection.getFieldValue(m, "a"), Direction.EAST,
			 * DigStatus.RELEASE_USE_ITEM); } } else if
			 * (Reflection.getFieldValue(m, "b").equals("east")) { if
			 * (Reflection.getFieldValue(m, "c").equals("START_DESTROY_BLOCK"))
			 * { p = new PacketPlayInBlockDig((BlockPosition)
			 * Reflection.getFieldValue(m, "a"), Direction.WEST,
			 * DigStatus.START_DESTROY_BLOCK); } else if
			 * (Reflection.getFieldValue(m, "c").equals("ABORT_DESTROY_BLOCK"))
			 * { p = new PacketPlayInBlockDig((BlockPosition)
			 * Reflection.getFieldValue(m, "a"), Direction.WEST,
			 * DigStatus.ABORT_DESTROY_BLOCK); } else if
			 * (Reflection.getFieldValue(m, "c").equals("STOP_DESTROY_BLOCK")) {
			 * p = new PacketPlayInBlockDig((BlockPosition)
			 * Reflection.getFieldValue(m, "a"), Direction.WEST,
			 * DigStatus.STOP_DESTROY_BLOCK); } else if
			 * (Reflection.getFieldValue(m, "c").equals("DROP_ALL_ITEMS")) { p =
			 * new PacketPlayInBlockDig((BlockPosition)
			 * Reflection.getFieldValue(m, "a"), Direction.WEST,
			 * DigStatus.DROP_ALL_ITEMS); } else if (Reflection.getFieldValue(m,
			 * "c").equals("DROP_ITEM")) { p = new
			 * PacketPlayInBlockDig((BlockPosition) Reflection.getFieldValue(m,
			 * "a"), Direction.WEST, DigStatus.DROP_ITEM); } else if
			 * (Reflection.getFieldValue(m, "c").equals("RELEASE_USE_ITEM")) { p
			 * = new PacketPlayInBlockDig((BlockPosition)
			 * Reflection.getFieldValue(m, "a"), Direction.WEST,
			 * DigStatus.RELEASE_USE_ITEM); } } else if
			 * (Reflection.getFieldValue(m, "b").equals("south")) { if
			 * (Reflection.getFieldValue(m, "c").equals("START_DESTROY_BLOCK"))
			 * { p = new PacketPlayInBlockDig((BlockPosition)
			 * Reflection.getFieldValue(m, "a"), Direction.NORTH,
			 * DigStatus.START_DESTROY_BLOCK); } else if
			 * (Reflection.getFieldValue(m, "c").equals("ABORT_DESTROY_BLOCK"))
			 * { p = new PacketPlayInBlockDig((BlockPosition)
			 * Reflection.getFieldValue(m, "a"), Direction.NORTH,
			 * DigStatus.ABORT_DESTROY_BLOCK); } else if
			 * (Reflection.getFieldValue(m, "c").equals("STOP_DESTROY_BLOCK")) {
			 * p = new PacketPlayInBlockDig((BlockPosition)
			 * Reflection.getFieldValue(m, "a"), Direction.NORTH,
			 * DigStatus.STOP_DESTROY_BLOCK); } else if
			 * (Reflection.getFieldValue(m, "c").equals("DROP_ALL_ITEMS")) { p =
			 * new PacketPlayInBlockDig((BlockPosition)
			 * Reflection.getFieldValue(m, "a"), Direction.NORTH,
			 * DigStatus.DROP_ALL_ITEMS); } else if (Reflection.getFieldValue(m,
			 * "c").equals("DROP_ITEM")) { p = new
			 * PacketPlayInBlockDig((BlockPosition) Reflection.getFieldValue(m,
			 * "a"), Direction.NORTH, DigStatus.DROP_ITEM); } else if
			 * (Reflection.getFieldValue(m, "c").equals("RELEASE_USE_ITEM")) { p
			 * = new PacketPlayInBlockDig((BlockPosition)
			 * Reflection.getFieldValue(m, "a"), Direction.NORTH,
			 * DigStatus.RELEASE_USE_ITEM); } } else if
			 * (Reflection.getFieldValue(m, "b").equals("north")) { if
			 * (Reflection.getFieldValue(m, "c").equals("START_DESTROY_BLOCK"))
			 * { p = new PacketPlayInBlockDig((BlockPosition)
			 * Reflection.getFieldValue(m, "a"), Direction.SOUTH,
			 * DigStatus.START_DESTROY_BLOCK); } else if
			 * (Reflection.getFieldValue(m, "c").equals("ABORT_DESTROY_BLOCK"))
			 * { p = new PacketPlayInBlockDig((BlockPosition)
			 * Reflection.getFieldValue(m, "a"), Direction.SOUTH,
			 * DigStatus.ABORT_DESTROY_BLOCK); } else if
			 * (Reflection.getFieldValue(m, "c").equals("STOP_DESTROY_BLOCK")) {
			 * p = new PacketPlayInBlockDig((BlockPosition)
			 * Reflection.getFieldValue(m, "a"), Direction.SOUTH,
			 * DigStatus.STOP_DESTROY_BLOCK); } else if
			 * (Reflection.getFieldValue(m, "c").equals("DROP_ALL_ITEMS")) { p =
			 * new PacketPlayInBlockDig((BlockPosition)
			 * Reflection.getFieldValue(m, "a"), Direction.SOUTH,
			 * DigStatus.DROP_ALL_ITEMS); } else if (Reflection.getFieldValue(m,
			 * "c").equals("DROP_ITEM")) { p = new
			 * PacketPlayInBlockDig((BlockPosition) Reflection.getFieldValue(m,
			 * "a"), Direction.SOUTH, DigStatus.DROP_ITEM); } else if
			 * (Reflection.getFieldValue(m, "c").equals("RELEASE_USE_ITEM")) { p
			 * = new PacketPlayInBlockDig((BlockPosition)
			 * Reflection.getFieldValue(m, "a"), Direction.SOUTH,
			 * DigStatus.RELEASE_USE_ITEM); } }
			 */
			PacketEvent<PacketPlayInBlockDig> pe = PacketAPI.getInstance().onPacket(player, p);
			if (!pe.isCancelled()) {
				super.channelRead(c, m);
			}
		} else if (m.getClass().getSimpleName().equalsIgnoreCase("PacketPlayInHeldItemSlot")) {
			PacketPlayInHeldItemSlot p = new PacketPlayInHeldItemSlot(
					(int) Reflection.getFieldValue(m, "itemInHandIndex"), m);
			PacketEvent<PacketPlayInHeldItemSlot> pe = PacketAPI.getInstance().onPacket(player, p);
			if (!pe.isCancelled()) {
				super.channelRead(c, m);
			}
		} else if (m.getClass().getSimpleName().equalsIgnoreCase("PacketPlayInArmAnimation")) {
			PacketPlayInArmAnimation p = new PacketPlayInArmAnimation((long) Reflection.getFieldValue(m, "timestamp"),
					m);
			PacketEvent<PacketPlayInArmAnimation> pe = PacketAPI.getInstance().onPacket(player, p);
			if (!pe.isCancelled()) {
				super.channelRead(c, m);
			}
		} else if (m.getClass().getSimpleName().equalsIgnoreCase("PacketPlayInResourcePackStatus")) {
			PacketPlayInResourcePackStatus p = new PacketPlayInResourcePackStatus(
					ResourcePackStatus.valueOf(Reflection.getFieldValue(m, "b").toString()), m);
			PacketEvent<PacketPlayInResourcePackStatus> pe = PacketAPI.getInstance().onPacket(player, p);
			if (!pe.isCancelled()) {
				super.channelRead(c, m);
			}
		} else if (m.getClass().getSimpleName().equalsIgnoreCase("PacketPlayInBlockPlace")) {
			PacketPlayInBlockPlace p = new PacketPlayInBlockPlace(
					Reflection.getLocationFromBlockPosition(Reflection.getFieldValue(m, "a"), player.getWorld()), null,
					m);
			PacketEvent<PacketPlayInBlockPlace> pe = PacketAPI.getInstance().onPacket(player, p);
			if (!pe.isCancelled()) {
				super.channelRead(c, m);
			}
		} else {
			Packet p = new Packet(PacketType.In.ALL, m);
			PacketEvent<Packet> pe = PacketAPI.getInstance().onPacket(player, p);
			if (!pe.isCancelled()) {
				super.channelRead(c, m);
			}
		}
	}

}
