package fr.adamaq01.packetapi.utils;

import javax.annotation.Nonnull;

import fr.adamaq01.packetapi.packets.Packet;
import fr.adamaq01.packetapi.packets.enums.PacketType;
import fr.adamaq01.packetapi.packets.events.PacketEvent;

public abstract class PacketHandler<T extends Packet> {
	
	private PacketType type;
	
	public PacketHandler(@Nonnull PacketType type) {
		this.type = type;
	}
	
	public PacketType getPacketType() {
		return type;
	}

	public abstract void onPacket(PacketEvent<T> event);
	
}