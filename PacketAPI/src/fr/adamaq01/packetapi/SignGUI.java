package fr.adamaq01.packetapi;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.plugin.Plugin;
import org.bukkit.util.Vector;

import fr.adamaq01.packetapi.packets.SendablePacket;
import fr.adamaq01.packetapi.packets.enums.PacketType;
import fr.adamaq01.packetapi.packets.events.PacketEvent;
import fr.adamaq01.packetapi.packets.in.PacketPlayInUpdateSign;
import fr.adamaq01.packetapi.packets.out.PacketPlayOutOpenSignEditor;
import fr.adamaq01.packetapi.packets.out.PacketPlayOutUpdateSign;
import fr.adamaq01.packetapi.utils.PacketHandler;

public class SignGUI {

	protected PacketHandler<PacketPlayInUpdateSign> packetHandler;
	protected Map<String, SignGUIListener> listeners;
	protected Map<String, Vector> signLocations;

	public SignGUI(final Plugin plugin) {
		listeners = new ConcurrentHashMap<String, SignGUIListener>();
		signLocations = new ConcurrentHashMap<String, Vector>();
		PacketAPI.addPacketListener(packetHandler = new PacketHandler<PacketPlayInUpdateSign>(PacketType.In.UPDATE_SIGN) {
			@Override
			public void onPacket(PacketEvent<PacketPlayInUpdateSign> event) {
				final Player player = event.getPlayer();
				PacketPlayInUpdateSign packet = event.getPacket();
				Vector v = signLocations.remove(player.getName());
				Location bp = packet.getLocation();
				final String[] lines = packet.getLines();
				final SignGUIListener response = listeners.remove(event.getPlayer().getName());

				if (v == null)
					return;
				if (bp.getX() != v.getBlockX())
					return;
				if (bp.getY() != v.getBlockY())
					return;
				if (bp.getZ() != v.getBlockZ())
					return;
				if (response != null) {
					event.setCancelled(true);
					Bukkit.getScheduler().scheduleSyncDelayedTask(plugin, new Runnable() {
						public void run() {
							response.onSignDone(player, lines);
						}
					});
				}
			}
		});
	}

	@SuppressWarnings("deprecation")
	public void open(Player player, String[] defaultText, SignGUIListener response) {
		List<SendablePacket> packets = new ArrayList<SendablePacket>();
		int x = player.getLocation().getBlockX();
		int y = 0;
		int z = player.getLocation().getBlockZ();
		Location bpos = new Location(player.getWorld(), x, y, z);
		PacketPlayOutOpenSignEditor packet133 = new PacketPlayOutOpenSignEditor(bpos);
		packets.add(packet133);
		if (defaultText != null) {
			String[] cc = { defaultText[0], defaultText[1], defaultText[2], defaultText[3] };
			PacketPlayOutUpdateSign packet130 = new PacketPlayOutUpdateSign(player.getWorld(), bpos, cc);

			packets.add(packet130);
		}
		player.sendBlockChange(new Location(player.getWorld(), x, y, z), Material.SIGN_POST, (byte) 0);
		for (SendablePacket packet : packets) {
			packet.send(player);
		}
		signLocations.put(player.getName(), new Vector(x, y, z));
		listeners.put(player.getName(), response);

	}

	public void destroy() {
		PacketAPI.removePacketListener(packetHandler);
		listeners.clear();
		signLocations.clear();
	}

	public interface SignGUIListener {
		public void onSignDone(Player player, String[] lines);
	}
}