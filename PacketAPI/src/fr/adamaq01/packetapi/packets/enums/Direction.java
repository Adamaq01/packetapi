package fr.adamaq01.packetapi.packets.enums;

public enum Direction {
	
	DOWN(0),
	UP(1),
	WEST(2),
	EAST(3),
	NORTH(4),
	SOUTH(5);
	
	private int id;
	
	private Direction(int id) {
		this.id = id;
	}
	
	public int getId() {
		return id;
	}

}
