package fr.adamaq01.packetapi.packets.enums;

public enum EntityUseAction {
	
	ATTACK,
	INTERACT,
	INTERACT_AT;

}
