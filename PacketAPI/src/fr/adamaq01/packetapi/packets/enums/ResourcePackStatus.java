package fr.adamaq01.packetapi.packets.enums;

public enum ResourcePackStatus {
	
	SUCCESSFULLY_LOADED, DECLINED, FAILED_DOWNLOAD, ACCEPTED;

}
