package fr.adamaq01.packetapi.packets.enums;

public interface PacketType {

	public String getName();

	public enum In implements PacketType {

		KEEP_ALIVE("PacketPlayInKeepAlive"), 
		CHAT("PacketPlayInChat"), 
		USE_ENTITY("PacketPlayInUseEntity"), 
		POSITION("PacketPlayInPosition"), 
		LOOK("PacketPlayInLook"), 
		POSITION_LOOK("PacketPlayInPositionLook"), 
		FLYING("PacketPlayInFlying"), 
		UPDATE_SIGN("PacketPlayInUpdateSign"), 
		BLOCK_DIG("PacketPlayInBlockDig"),
		BLOCK_PLACE("PacketPlayInBlockPlace"),
		HELD_ITEM_CHANGE("PacketPlayInHeldItemSlot"), 
		ARM_ANIMATION("PacketPlayInArmAnimation"), 
		RESOURCE_PACK_STATUS("PacketPlayInResourcePackStatus"), 
		ALL("All");

		private String name;

		private In(String name) {
			this.name = name;
		}

		@Override
		public String getName() {
			return this.name;
		}

		public static boolean containsPacket(String packetName) {
			for (PacketType.In type : PacketType.In.values()) {
				if (type.getName().equals(packetName)) {
					return true;
				} else {
					return false;
				}
			}
			return false;
		}

		public static PacketType.In getByName(String packetName) {
			for (PacketType.In type : PacketType.In.values()) {
				if (type.getName().equals(packetName)) {
					return type;
				} else {
					return type;
				}
			}
			return null;
		}

	}

	public enum Out implements PacketType {

		ANIMATION("PacketPlayOutAnimation"), 
		CAMERA("PacketPlayOutCamera"), 
		OPEN_SIGN_EDITOR("PacketPlayOutOpenSignEditor"), 
		UPDATE_SIGN("PacketPlayOutUpdateSign"), 
		ALL("All");

		private String name;

		private Out(String name) {
			this.name = name;
		}

		@Override
		public String getName() {
			return this.name;
		}

		public static boolean containsPacket(String packetName) {
			for (PacketType.Out type : PacketType.Out.values()) {
				if (type.getName().equals(packetName)) {
					return true;
				} else {
					return false;
				}
			}
			return false;
		}

		public static PacketType.Out getByName(String packetName) {
			for (PacketType.Out type : PacketType.Out.values()) {
				if (type.getName().equals(packetName)) {
					return type;
				} else {
					return type;
				}
			}
			return null;
		}

	}

}
