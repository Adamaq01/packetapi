package fr.adamaq01.packetapi.packets.enums;

public enum Animation {
	
	SWING_ARM(0),
	TAKE_DAMAGE(1),
	LEAVE_BED(2),
	EAT_FOOD(3),
	CRITICAL_EFFECT(4),
	MAGIC_CRITICAL_EFFECT(5);
	
	private int id;
	
	private Animation(int id) {
		this.id = id;
	}
	
	public int getId() {
		return id;
	}

}
