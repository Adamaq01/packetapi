package fr.adamaq01.packetapi.packets.enums;

public enum Dimension {

	OVERWORLD(0), NETHER(-1), END(1), UNKNOWN(999);

	private int id;

	private Dimension(int id) {
		this.id = id;
	}

	public int getId() {
		return id;
	}

	public static Dimension getById(int id) {
		if (id == -1) {
			return Dimension.NETHER;
		} else if (id == 0) {
			return Dimension.OVERWORLD;
		} else if (id == 1) {
			return Dimension.END;
		} else {
			return Dimension.UNKNOWN;
		}
	}

}
