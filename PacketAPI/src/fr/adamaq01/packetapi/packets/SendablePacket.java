package fr.adamaq01.packetapi.packets;

import java.util.ArrayList;
import java.util.Collection;

import org.bukkit.entity.Player;

public interface SendablePacket {
	
	public void send(Player player);
	
	public void send(Player... player);
	
	public void send(ArrayList<Player> players);
	
	public void send(Collection<? extends Player> players);
	
}
