package fr.adamaq01.packetapi.packets;

import fr.adamaq01.packetapi.packets.enums.PacketType;
import fr.adamaq01.packetapi.utils.Reflection;

public class Packet {

	private PacketType type;
	private Object packet;

	public Packet(PacketType type, Object packet) {
		this.type = type;
		this.packet = packet;
	}

	public PacketType getPacketType() {
		return type;
	}

	public Object getNMSPacket() {
		return packet;
	}

	public Object getPacketValue(String field) {
		try{
			return Reflection.getFieldValue(packet, field);
		} catch(Exception e) {
			return Reflection.getProtectedFieldValue(packet, field);
		}
	}

}
