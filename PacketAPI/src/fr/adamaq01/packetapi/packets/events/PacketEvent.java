package fr.adamaq01.packetapi.packets.events;

import org.bukkit.entity.Player;

import fr.adamaq01.packetapi.packets.Packet;

public class PacketEvent<T extends Packet> {

	private T packet;
	private Player player;
	private boolean cancelled = false;

	public PacketEvent(T packet, Player player) {
		this.packet = packet;
		this.player = player;
	}

	public T getPacket() {
		return packet;
	}

	public Player getPlayer() {
		return player;
	}

	public void setCancelled(boolean cancelled) {
		this.cancelled = cancelled;
	}

	public boolean isCancelled() {
		return cancelled;
	}

}
