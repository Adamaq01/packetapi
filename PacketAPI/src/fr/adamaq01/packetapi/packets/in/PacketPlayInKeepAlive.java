package fr.adamaq01.packetapi.packets.in;

import fr.adamaq01.packetapi.packets.Packet;
import fr.adamaq01.packetapi.packets.enums.PacketType;

public class PacketPlayInKeepAlive extends Packet {

	private int fieldA;

	public PacketPlayInKeepAlive(int fieldA, Object packet) {
		super(PacketType.In.KEEP_ALIVE, packet);
		this.fieldA = fieldA;
	}

	public int getKeepAliveId() {
		return fieldA;
	}

}
