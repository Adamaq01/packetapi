package fr.adamaq01.packetapi.packets.in;

import fr.adamaq01.packetapi.packets.enums.PacketType;

public class PacketPlayInPositionLook extends PacketPlayInFlying {

	public PacketPlayInPositionLook(double fieldX, double fieldY, double fieldZ, float fieldYaw, float fieldPitch,
			boolean onGround, boolean hasLook, boolean hasPosition, Object packet) {
		super(PacketType.In.POSITION_LOOK, fieldX, fieldY, fieldZ, fieldYaw, fieldPitch, onGround, hasLook, hasPosition, packet);
	}

}
