package fr.adamaq01.packetapi.packets.in;

import fr.adamaq01.packetapi.packets.Packet;
import fr.adamaq01.packetapi.packets.enums.PacketType;

public class PacketPlayInHeldItemSlot extends Packet {

	private int slot;
	
	public PacketPlayInHeldItemSlot(int slot, Object packet) {
		super(PacketType.In.HELD_ITEM_CHANGE, packet);
		this.slot = slot;
	}
	
	public int getSlot() {
		return slot;
	}

}
