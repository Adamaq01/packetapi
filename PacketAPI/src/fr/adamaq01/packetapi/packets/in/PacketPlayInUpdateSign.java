package fr.adamaq01.packetapi.packets.in;

import org.bukkit.Location;

import fr.adamaq01.packetapi.packets.Packet;
import fr.adamaq01.packetapi.packets.enums.PacketType;

public class PacketPlayInUpdateSign extends Packet {
	
	private String[] fieldB;
	private Location fieldA;

	public PacketPlayInUpdateSign(String[] fieldB, Location fieldA, Object packet) {
		super(PacketType.In.UPDATE_SIGN, packet);
		this.fieldB = fieldB;
		this.fieldA = fieldA;
	}
	
	public String[] getLines() {
		return fieldB;
	}
	
	public Location getLocation() {
		return fieldA;
	}

}
