package fr.adamaq01.packetapi.packets.in;

import fr.adamaq01.packetapi.packets.Packet;
import fr.adamaq01.packetapi.packets.enums.PacketType;
import fr.adamaq01.packetapi.packets.enums.ResourcePackStatus;

public class PacketPlayInResourcePackStatus extends Packet {

	private ResourcePackStatus status;
	
	public PacketPlayInResourcePackStatus(ResourcePackStatus resourcePackStatus, Object packet) {
		super(PacketType.In.RESOURCE_PACK_STATUS, packet);
		this.status = resourcePackStatus;
	}
	
	public ResourcePackStatus getStatus() {
		return status;
	}

}
