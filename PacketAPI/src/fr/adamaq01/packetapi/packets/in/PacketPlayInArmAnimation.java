package fr.adamaq01.packetapi.packets.in;

import fr.adamaq01.packetapi.packets.Packet;
import fr.adamaq01.packetapi.packets.enums.PacketType;

public class PacketPlayInArmAnimation extends Packet{
	
	private long timestamp;

	public PacketPlayInArmAnimation(long timestamp, Object packet) {
		super(PacketType.In.ARM_ANIMATION, packet);
		this.timestamp = timestamp;
	}
	
	public long getTimeStamp() {
		return timestamp;
	}
	
}
