package fr.adamaq01.packetapi.packets.in;

import fr.adamaq01.packetapi.packets.Packet;
import fr.adamaq01.packetapi.packets.enums.PacketType;

public class PacketPlayInFlying extends Packet {

	private double fieldX;
	private double fieldY;
	private double fieldZ;
	private float fieldYaw;
	private float fieldPitch;
	private boolean onGround;
	private boolean hasLook;
	private boolean hasPosition;

	protected PacketPlayInFlying(PacketType type, double fieldX, double fieldY, double fieldZ, float fieldYaw, float fieldPitch,
			boolean onGround, boolean hasLook, boolean hasPosition, Object packet) {
		super(type, packet);
		this.fieldX = fieldX;
		this.fieldY = fieldY;
		this.fieldZ = fieldZ;
		this.fieldYaw = fieldYaw;
		this.fieldPitch = fieldPitch;
		this.onGround = onGround;
		this.hasLook = hasPosition;
		this.hasPosition = hasPosition;
	}
	
	public PacketPlayInFlying(double fieldX, double fieldY, double fieldZ, float fieldYaw, float fieldPitch,
			boolean onGround, boolean hasLook, boolean hasPosition, Object packet) {
		super(PacketType.In.FLYING, packet);
		this.fieldX = fieldX;
		this.fieldY = fieldY;
		this.fieldZ = fieldZ;
		this.fieldYaw = fieldYaw;
		this.fieldPitch = fieldPitch;
		this.onGround = onGround;
		this.hasLook = hasPosition;
		this.hasPosition = hasPosition;
	}

	public double getX() {
		return fieldX;
	}

	public double getY() {
		return fieldY;
	}

	public double getZ() {
		return fieldZ;
	}

	public float getYaw() {
		return fieldYaw;
	}

	public float getPitch() {
		return fieldPitch;
	}

	public boolean isOnGround() {
		return onGround;
	}

	public boolean hasLook() {
		return hasLook;
	}

	public boolean hasPosition() {
		return hasPosition;
	}

}
