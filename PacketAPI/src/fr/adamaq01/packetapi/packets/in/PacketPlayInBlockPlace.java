package fr.adamaq01.packetapi.packets.in;

import org.bukkit.Location;

import fr.adamaq01.packetapi.packets.Packet;
import fr.adamaq01.packetapi.packets.enums.DigStatus;
import fr.adamaq01.packetapi.packets.enums.PacketType;

public class PacketPlayInBlockPlace extends Packet {

	private Location loc;
	private DigStatus status;
	
	public PacketPlayInBlockPlace(Location loc, DigStatus status, Object packet) {
		super(PacketType.In.BLOCK_PLACE, packet);
		this.loc = loc;
		this.status = status;
	}
	
	public Location getLocation() {
		return loc;
	}

	@Deprecated
	public DigStatus getStatus() {
		return status;
	}

}
