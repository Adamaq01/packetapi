package fr.adamaq01.packetapi.packets.in;

import org.bukkit.Bukkit;
import org.bukkit.World;
import org.bukkit.entity.Entity;

import fr.adamaq01.packetapi.packets.Packet;
import fr.adamaq01.packetapi.packets.enums.EntityUseAction;
import fr.adamaq01.packetapi.packets.enums.PacketType;

public class PacketPlayInUseEntity extends Packet {

	private int fieldTarget;
	private EntityUseAction fieldType;

	public PacketPlayInUseEntity(int fieldTarget, EntityUseAction fieldType, Object packet) {
		super(PacketType.In.USE_ENTITY, packet);
		this.fieldTarget = fieldTarget;
		this.fieldType = fieldType;
	}

	public EntityUseAction getType() {
		return fieldType;
	}
	
	public int getTargetId() {
		return fieldTarget;
	}

	public Entity getTarget() {
		Entity returnEnt = null;
		for (World w : Bukkit.getWorlds()) {
			for (Entity ent : w.getEntities()) {
				if (ent.getEntityId() == fieldTarget) {
					returnEnt = ent;
					break;
				}
			}
		}
		return returnEnt;
	}

}
