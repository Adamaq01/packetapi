package fr.adamaq01.packetapi.packets.in;

import org.bukkit.Location;

import fr.adamaq01.packetapi.packets.Packet;
import fr.adamaq01.packetapi.packets.enums.DigStatus;
import fr.adamaq01.packetapi.packets.enums.Direction;
import fr.adamaq01.packetapi.packets.enums.PacketType;

public class PacketPlayInBlockDig extends Packet {

	private Location fieldA;
	private Direction fieldB;
	private DigStatus fieldC;

	public PacketPlayInBlockDig(Location fieldA, Direction fieldB, DigStatus fieldC, Object packet) {
		super(PacketType.In.BLOCK_DIG, packet);
		this.fieldA = fieldA;
		this.fieldB = fieldB;
		this.fieldC = fieldC;
	}

	public Location getLocation() {
		return fieldA;
	}

	@Deprecated
	public Direction getDirection() {
		return fieldB;
	}

	@Deprecated
	public DigStatus getStatus() {
		return fieldC;
	}

}
