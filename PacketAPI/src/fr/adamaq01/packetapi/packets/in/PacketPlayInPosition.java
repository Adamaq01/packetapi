package fr.adamaq01.packetapi.packets.in;

import fr.adamaq01.packetapi.packets.enums.PacketType;

public class PacketPlayInPosition extends PacketPlayInFlying {

	public PacketPlayInPosition(double fieldX, double fieldY, double fieldZ, float fieldYaw, float fieldPitch,
			boolean onGround, boolean hasLook, boolean hasPosition, Object packet) {
		super(PacketType.In.POSITION, fieldX, fieldY, fieldZ, fieldYaw, fieldPitch, onGround, hasLook, hasPosition, packet);
	}

}
