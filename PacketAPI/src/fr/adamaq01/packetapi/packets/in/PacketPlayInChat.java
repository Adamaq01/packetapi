package fr.adamaq01.packetapi.packets.in;

import fr.adamaq01.packetapi.packets.Packet;
import fr.adamaq01.packetapi.packets.enums.PacketType;

public class PacketPlayInChat extends Packet {

	private String fieldA;

	public PacketPlayInChat(String fieldA, Object packet) {
		super(PacketType.In.CHAT, packet);
		this.fieldA = fieldA;
	}

	public String getMessage() {
		return fieldA;
	}

}
