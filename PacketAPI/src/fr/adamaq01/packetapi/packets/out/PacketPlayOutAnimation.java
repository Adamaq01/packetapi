package fr.adamaq01.packetapi.packets.out;

import java.util.ArrayList;
import java.util.Collection;

import org.bukkit.Bukkit;
import org.bukkit.World;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Player;

import fr.adamaq01.packetapi.packets.Packet;
import fr.adamaq01.packetapi.packets.SendablePacket;
import fr.adamaq01.packetapi.packets.enums.Animation;
import fr.adamaq01.packetapi.packets.enums.PacketType;
import fr.adamaq01.packetapi.utils.Reflection;

public class PacketPlayOutAnimation extends Packet implements SendablePacket {

	private int fieldA;
	private Animation fieldB;

	public PacketPlayOutAnimation(int playerid, Animation animation, Object packet) {
		super(PacketType.Out.ANIMATION, packet);
		this.fieldA = playerid;
		this.fieldB = animation;
	}
	
	public PacketPlayOutAnimation(int playerid, Animation animation) {
		super(PacketType.Out.ANIMATION, null);
		this.fieldA = playerid;
		this.fieldB = animation;
	}

	public int getEntityId() {
		return fieldA;
	}

	public Player getPlayer() {
		Player returnPlayer = null;
		for (World w : Bukkit.getWorlds()) {
			for (Entity ent : w.getEntities()) {
				if (ent.getEntityId() == fieldA && ent instanceof Player) {
					returnPlayer = (Player) ent;
					break;
				}
			}
		}
		return returnPlayer;
	}

	public Animation getAnimation() {
		return fieldB;
	}

	@Override
	public void send(Player player) {
		try {
			Object packet = Reflection.getPacket(PacketType.Out.ANIMATION.getName(), Reflection.getNmsPlayer(player),
					fieldB.getId());
			Reflection.sendPlayerPacket(player, packet);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Override
	public void send(Player... player) {
		for (Player pls : player) {
			send(pls);
		}
	}

	@Override
	public void send(ArrayList<Player> players) {
		for (Player pls : players) {
			send(pls);
		}
	}

	@Override
	public void send(Collection<? extends Player> players) {
		for (Player pls : players) {
			send(pls);
		}
	}

}
