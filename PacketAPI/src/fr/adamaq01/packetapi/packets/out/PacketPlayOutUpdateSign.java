package fr.adamaq01.packetapi.packets.out;

import java.lang.reflect.Constructor;
import java.util.ArrayList;
import java.util.Collection;

import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.craftbukkit.v1_8_R3.CraftWorld;
import org.bukkit.entity.Player;

import fr.adamaq01.packetapi.packets.Packet;
import fr.adamaq01.packetapi.packets.SendablePacket;
import fr.adamaq01.packetapi.packets.enums.PacketType;
import fr.adamaq01.packetapi.utils.Reflection;

public class PacketPlayOutUpdateSign extends Packet implements SendablePacket {

	private World fieldA;
	private Location fieldB;
	private String[] fieldC;

	public PacketPlayOutUpdateSign(World world, Location blockPosition, String[] baseComponents, Object packet) {
		super(PacketType.Out.UPDATE_SIGN, packet);
		this.fieldA = world;
		this.fieldB = blockPosition;
		this.fieldC = baseComponents;
	}
	
	public PacketPlayOutUpdateSign(World world, Location blockPosition, String[] baseComponents) {
		super(PacketType.Out.UPDATE_SIGN, null);
		this.fieldA = world;
		this.fieldB = blockPosition;
		this.fieldC = baseComponents;
	}

	public World getWorld() {
		return fieldA;
	}

	public Location getLocation() {
		return fieldB;
	}

	public String[] getLines() {
		return fieldC;
	}

	@Override
	public void send(Player player) {
		try {
			Class<?> packetClass = Reflection.getClass("{nms}." + PacketType.Out.UPDATE_SIGN.getName());
			Constructor<?> packetConstruct = packetClass.getDeclaredConstructors()[0];
			Object packet = packetConstruct.newInstance(((CraftWorld) fieldA).getHandle(), fieldB, fieldC);
			Reflection.sendPlayerPacket(player, packet);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Override
	public void send(Player... player) {
		for (Player pls : player) {
			send(pls);
		}
	}

	@Override
	public void send(ArrayList<Player> players) {
		for (Player pls : players) {
			send(pls);
		}
	}
	
	@Override
	public void send(Collection<? extends Player> players) {
		for (Player pls : players) {
			send(pls);
		}
	}

}
