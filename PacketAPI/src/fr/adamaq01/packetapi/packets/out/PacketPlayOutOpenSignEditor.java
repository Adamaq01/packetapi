package fr.adamaq01.packetapi.packets.out;

import java.util.ArrayList;
import java.util.Collection;

import org.bukkit.Location;
import org.bukkit.entity.Player;

import fr.adamaq01.packetapi.packets.Packet;
import fr.adamaq01.packetapi.packets.SendablePacket;
import fr.adamaq01.packetapi.packets.enums.PacketType;
import fr.adamaq01.packetapi.utils.Reflection;

public class PacketPlayOutOpenSignEditor extends Packet implements SendablePacket {

	private Location fieldA;

	public PacketPlayOutOpenSignEditor(Location a, Object packet) {
		super(PacketType.Out.OPEN_SIGN_EDITOR, packet);
		this.fieldA = a;
	}
	
	public PacketPlayOutOpenSignEditor(Location a) {
		super(PacketType.Out.OPEN_SIGN_EDITOR, null);
		this.fieldA = a;
	}

	public Location getLocation() {
		return fieldA;
	}

	@Override
	public void send(Player player) {
		try {
			Object packet = Reflection.getPacket(PacketType.Out.OPEN_SIGN_EDITOR.getName(),
					Reflection.getBlockPositionFromLocation(fieldA));
			Reflection.sendPlayerPacket(player, packet);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Override
	public void send(Player... player) {
		for (Player pls : player) {
			send(pls);
		}
	}

	@Override
	public void send(ArrayList<Player> players) {
		for (Player pls : players) {
			send(pls);
		}
	}
	
	@Override
	public void send(Collection<? extends Player> players) {
		for (Player pls : players) {
			send(pls);
		}
	}

}
