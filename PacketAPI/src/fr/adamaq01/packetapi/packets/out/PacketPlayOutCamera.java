package fr.adamaq01.packetapi.packets.out;

import java.util.ArrayList;
import java.util.Collection;

import org.bukkit.entity.Entity;
import org.bukkit.entity.Player;

import fr.adamaq01.packetapi.packets.Packet;
import fr.adamaq01.packetapi.packets.SendablePacket;
import fr.adamaq01.packetapi.packets.enums.PacketType;
import fr.adamaq01.packetapi.utils.Reflection;

public class PacketPlayOutCamera extends Packet implements SendablePacket {

	private Entity fieldA;

	public PacketPlayOutCamera(Entity camera, Object packet) {
		super(PacketType.Out.CAMERA, packet);
		this.fieldA = camera;
	}
	
	public PacketPlayOutCamera(Entity camera) {
		super(PacketType.Out.CAMERA, null);
		this.fieldA = camera;
	}

	public Entity getCamera() {
		return fieldA;
	}

	@Override
	public void send(Player player) {
		Object packet = Reflection.getPacket(PacketType.Out.CAMERA.getName(), fieldA.getEntityId());
		Reflection.sendPlayerPacket(player, packet);
	}

	@Override
	public void send(Player... player) {
		for (Player pls : player) {
			send(pls);
		}
	}

	@Override
	public void send(ArrayList<Player> players) {
		for (Player pls : players) {
			send(pls);
		}
	}
	
	@Override
	public void send(Collection<? extends Player> players) {
		for (Player pls : players) {
			send(pls);
		}
	}

}
